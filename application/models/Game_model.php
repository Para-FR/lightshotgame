<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
		$this->encryption->initialize(
			array(
				'cipher' => 'aes-256',
				'mode' => 'ctr',
				'key' => 'P@raC0urses2020'
			)
		);
	}
    public function getImage($select, $where = null, $value = null, $returnType = 'array') {

        //die(var_dump($returnType));

        $this->db->select($select)
            ->from('images')
            ->order_by('img_options', 'ASC');

        if ($where != null && $value != null) {
            $this->db->where($where, $value);
        }

        $result = $this->db->get();
        if ($returnType === 'row') {

            if ($result->num_rows() > 0) {
                return $result->row();
            } else {
                return false;
            }
        } else {

            if ($result->num_rows() > 0) {
                return $result->result();
            } else {

                return false;
            }
        }
    }

    public function addVote($img_id, $ip_user) {

	    //$ip_user = '91.121.136.10';

        // Récupération du nombre de votes
	    $nbVotes = $this->db->select('img_options')
            ->from('images')
            ->where('id', $img_id)
            ->get()
            ->row()->img_options;

        // On recherche l'ip et l'id de l'image

        $checkVote = $this->db->select('*')
            ->from('ranking')
            ->where('rank_img_id', $img_id)
            ->where('rank_use_ip', $ip_user)
            ->get();

        //die(var_dump($checkVote));

        if ($checkVote->num_rows() > 0) {
           return false;
        } else {
            // On met à jour la table ranking (INSERT)
            $data['rank_use_ip'] = $ip_user;
            $data['rank_img_id'] = $img_id;
            $data['rank_vote'] = 1;

            $this->db->insert('ranking', $data);

            // On met à jour la table images
            $content['img_options'] = $nbVotes + 1;
            //die(var_dump($content));

            $this->db->where('id', $img_id)
                ->update('images', $content);

            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }

        // On vérifie si l'utilisateur a déjà voté

        //if()

	    //die(var_dump($nbVotes));
        /*$this->db->where('id', $img_id)
            ->update('images', )*/

    }
    public function minusVote($img_id, $ip_user)
    {

        $nbVotes = $this->db->select('img_options')
            ->from('images')
            ->where('id', $img_id)
            ->get()
            ->row()->img_options;


        $checkVote = $this->db->select('*')
            ->from('ranking')
            ->where('rank_img_id', $img_id)
            ->where('rank_use_ip', $ip_user)
            ->get();

        if ($checkVote->num_rows() > 0) {
            return false;
        } else {
            // On met à jour la table ranking (INSERT)
            $data['rank_use_ip'] = $ip_user;
            $data['rank_img_id'] = $img_id;
            $data['rank_vote'] = 1;

            $this->db->insert('ranking', $data);

            // On met à jour la table images
            $content['img_options'] = $nbVotes - 1;
            //die(var_dump($content));

            $this->db->where('id', $img_id)
                ->update('images', $content);

            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }


        }

    }



}
