<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }

    public function getHome($select)
    {

        $result=  $this->db->select($select)
            ->from('home h')

            ->get()->row();

        //die(var_dump($result));
        return $result;
    }




}


?>