<?php
/**
 * Created by PhpStorm.
 * User: Para
 * Date: 15/05/2018
 * Time: 15:53
 */
defined('BASEPATH') OR exit('No direct script allowed');
$lang['lang'] = 'fr';
$lang['hello'] = 'Bonjour';
$lang['home'] = 'Accueil';
$lang['play'] = 'Jouer';
$lang['ranking'] = 'Classement';
$lang['account'] = 'Mon Compte';
