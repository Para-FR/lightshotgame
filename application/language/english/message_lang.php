<?php
/**
 * Created by PhpStorm.
 * User: Para
 * Date: 15/05/2018
 * Time: 15:53
 */
defined('BASEPATH') OR exit('No direct script allowed');
$lang['lang'] = 'en';
$lang['hello'] = 'Hello';
$lang['home'] = 'Home';
$lang['play'] = 'Play';
$lang['ranking'] = 'Ranking';
$lang['account'] = 'My Account';
