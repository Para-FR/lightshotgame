<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home_model', 'homeManager');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        // Chargement des CSS
        $this->data['css'] = $this->layout->add_css(array(
            'assets/plugins/bootstrap/css/bootstrap.min',
            'assets/plugins/elegant_font/css/style',
            'assets/css/styles',
            'assets/css/home'
        ));
        // Chargement des JS
        $this->data['js'] = $this->layout->add_js(array(
            'assets/plugins/jquery-3.3.1.min',
            'assets/plugins/bootstrap/js/bootstrap.min',
            'assets/js/home'
        ));

        // Chargement de la vue
        $this->data['subview'] = 'home/main';

       $this->data['home'] = $this->homeManager->getHome('*');
       //$this->data['homepopup'] = $this->homeManager->getRanking('content_popup');

        $this->load->view('components_home/main', $this->data);

        //$this->data['test'] = 'une donnée';

        //$this->layout->view('index', $this->data);

        // die(var_dump($test));

        // $this->load->view('index');
    }
}
