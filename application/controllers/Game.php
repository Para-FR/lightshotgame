<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Game_model', 'gameManager');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
        {
            // Chargement des CSS
            $this->data['css'] = $this->layout->add_css(array(
                'assets/plugins/bootstrap/css/bootstrap.min',
                'assets/plugins/elegant_font/css/style',
                'assets/css/styles'
            ));
            // Chargement des JS
            $this->data['js'] = $this->layout->add_js(array(
                'assets/plugins/jquery-3.3.1.min',
                'assets/plugins/bootstrap/js/bootstrap.min',
                'assets/js/game',
            ));

            // Chargement de la vue
            $this->data['subview'] = 'game/main';

            //$image = $this->data['images'] = $this->gameManager->getImage('*');
            $maxId = count($this->gameManager->getImage('*'));

            $randomMaxId = rand(1, $maxId);


            $randomImage = $this->gameManager->getImage('*','id',$randomMaxId,'row');

            $this->data['image'] = $randomImage;








            // Définition de la variable pour voir si l'utilisateur est connecté
		// $loggedInCheck = $this->checkIfLoggedIn();

		// die(var_dump($loggedInCheck));

		// Si l'utilisateur est connecté on affiche un header différent
		/* if ($loggedInCheck) {

		}*/

		$this->load->view('components_home/main', $this->data);

		//$this->data['test'] = 'une donnée';

		//$this->layout->view('index', $this->data);

		// die(var_dump($test));

		// $this->load->view('index');
	}

    function get_client_ip_env() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

	public function vote() {

	    // Récupération de l'id de l'image par l'AJAX via la méthode POST
	    $img_id = $this->input->post('id');
	    // Récupération de l'adresse ip de l'utilisateur
	    $ip_user = $this->get_client_ip_env();

	   // On récupère une l'image depuis la BDD
        $randomImage = $this->gameManager->getImage( '*' , 'id' , $img_id , 'row' );

        // SI l'image existe
        if ($randomImage) {
            // Alors on entame le process de vote
            if ($this->gameManager->addVote($img_id, $ip_user)) {
                header('Content-Type:application/json');
                echo json_encode(
                    array(
                        'success' => 'Liké'
                    )
                );
            } else {
                header('Content-Type:application/json');
                echo json_encode(
                    array(
                        'error' => 'Disliké'
                    )
                );
            }
        } else {
            redirect(base_url('play'));
        }
    }
    public function voteminus() {

        // Récupération de l'id de l'image par l'AJAX via la méthode POST
        $img_id = $this->input->post('id');
        // Récupération de l'adresse ip de l'utilisateur
        $ip_user = $this->get_client_ip_env();

        // On récupère une l'image depuis la BDD
        $randomImage = $this->gameManager->getImage( '*' , 'id' , $img_id , 'row' );

        // SI l'image existe
        if ($randomImage) {
            // Alors on entame le process de vote
            if ($this->gameManager->minusVote($img_id, $ip_user)) {
                header('Content-Type:application/json');
                echo json_encode(
                    array(
                        'success' => 'Vote moins pris en compte'
                    )
                );
            } else {
                header('Content-Type:application/json');
                echo json_encode(
                    array(
                        'error' => 'Déjà voté'
                    )
                );
            }
        } else {
            redirect(base_url('play'));
        }
    }
}
