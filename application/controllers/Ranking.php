<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ranking extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ranking_model', 'rankingManager');
        $this->load->model('category_model', 'categoryManager');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */


    public function index()
    {
        // Chargement des CSS
        $this->data['css'] = $this->layout->add_css(array(
            'assets/plugins/bootstrap/css/bootstrap.min',
            'assets/plugins/elegant_font/css/style',
            'assets/css/styles'
        ));
        // Chargement des JS
        $this->data['js'] = $this->layout->add_js(array(
            'assets/plugins/jquery-3.3.1.min',
            'assets/plugins/bootstrap/js/bootstrap.min',
            'assets/js/rankingCategory',
        ));

        // Chargement de la vue
        $this->data['subview'] = 'ranking/main';

        // Définition de la variable pour voir si l'utilisateur est connecté
        // $loggedInCheck = $this->checkIfLoggedIn();

        // die(var_dump($loggedInCheck));

        // Si l'utilisateur est connecté on affiche un header différent
        /* if ($loggedInCheck) {

        }*/
        $this->data['images'] = $this->rankingManager->getRanking('*');
        //die(var_dump($this->data['images']  ));
        $this->load->view('components_home/main', $this->data);

        //$this->data['test'] = 'une donnée';

        //$this->layout->view('index', $this->data);

        // die(var_dump($test));

        // $this->load->view('index');
    }

    public function picture($namePicture){

        // Chargement des CSS
        $this->data['css'] = $this->layout->add_css(array(
            'assets/plugins/bootstrap/css/bootstrap.min',
            'assets/plugins/elegant_font/css/style',
            'assets/css/styles'
        ));
        // Chargement des JS
        $this->data['js'] = $this->layout->add_js(array(
            'assets/plugins/jquery-3.3.1.min',
            'assets/plugins/bootstrap/js/bootstrap.min',
            'assets/js/rankingCategory',
        ));



        //die(var_dump($namePicture));

        // Chargement de la vue
        $this->data['subview'] = 'ranking/picture/main'; // Je charge la nouvelle vue une fois cliqué sur le lien




        // Récupérer l'image et la stocker dans une variable
        $picture = $this->rankingManager->getRanking('*','img_name',$namePicture,'row');
        // Vérifier si l'image existe
        if ($picture){

            $this->data['picture'] = $picture; // Affiche seulement les données correspondant a la colonne choisis

        }else{
            redirect(base_url('ranking'));
        }

        $this->load->view('components_home/main', $this->data);

}







    public function getViewCategory (){ //Afficher la vue

        $dataRecues = $this->input->post();

        $rulesArray = array(
            array(
                'field' => 'classs_id',
                'label' => 'id',
                'rules' => 'trim|required|integer'
            )
        );

        $this->form_validation->set_rules($rulesArray);

        if ($this->form_validation->run() === FALSE) {
            //echo 'Erreur';
            header('Content-type:application/json');
            echo json_encode(array(
                'error' => "Un problème est survenue"
            ));

        } else {
            // Je récupère toutes les infos avec l'id
            $this->data['ranking'] = $this->rankingManager->getRanking('*', 'id', $this->input->post('classs_id'), $returnType = 'row');

            $this->data['category'] = $this->categoryManager->getCategory('*');

            $view = $this->load->view('ranking/category/main', $this->data, true); // boolean true pour permet de mettre la view dans une variable
            //renvoie en JSON succes + view
            header('Content-type:application/json');
            echo json_encode(array(
                'view' => $view,
            ));
        }
    }

}

