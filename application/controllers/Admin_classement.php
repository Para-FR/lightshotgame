<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_classement extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ranking_model', 'rankingManager');
        $this->load->model('edit_model', 'edit');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        // Chargement des CSS
        $this->data['css'] = $this->layout->add_css(array(
            'assets/plugins/bootstrap/css/bootstrap.min',
            'assets/plugins/elegant_font/css/style',
            'assets/css/styles'
        ));
        // Chargement des JS
        $this->data['js'] = $this->layout->add_js(array(
            'assets/plugins/jquery-3.3.1.min',
            'assets/plugins/bootstrap/js/bootstrap.min',
            'assets/js/classementPicture',
        ));

        // Chargement de la vue
        $this->data['subview'] = 'administration/ranking/main';

        $this->data['images'] = $this->rankingManager->getRanking('*');

        $this->load->view('components_home/main', $this->data);
    }

    public function getViewEdit (){ //Afficher la vue

        $dataRecues = $this->input->post();

        $rulesArray = array(
            array(
                'field' => 'classs_id',
                'label' => 'id',
                'rules' => 'trim|required|integer'
            )
        );

        $this->form_validation->set_rules($rulesArray);

        if ($this->form_validation->run() === FALSE) {
            //echo 'Erreur';
            header('Content-type:application/json');
            echo json_encode(array(
                'error' => "Un problème est survenue"
            ));

        } else {
            // Je récupère toutes les infos avec l'id
            $this->data['classement'] = $this->rankingManager->getRanking('*', 'id', $this->input->post('classs_id'), $returnType = 'row');
            $view = $this->load->view('administration/ranking/viewEdit', $this->data, true); // boolean true pour permet de mettre la view dans une variable
            //renvoie en JSON succes + view
            header('Content-type:application/json');
            echo json_encode(array(
                'view' => $view,
            ));
        }
    }



    public function edit() {


        $dataRecues = $this->input->post();
        //die(var_dump($dataRecues));
        $rulesArray = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'url_edit',
                'label' => 'competence',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'option_edit',
                'label' => 'pourcentage',
                'rules' => 'trim|required',
            ),
        );

        //die(var_dump($rulesArray));

        $this->form_validation->set_rules($rulesArray);

        if ($this->form_validation->run() === FALSE) {
            //echo 'Erreur';
            $errorsArray = $this->form_validation->get_all_errors();

            header('Content-type:application/json');
            echo json_encode(array(
                'error' => $errorsArray
            ));
            die();

        } else {

            $dataUpdate = array(
                "img_url" => $this->input->post('url_edit'),
                "img_options" => $this->input->post('option_edit'),
            );
            $dataWhere = array(
                'id' => $this->input->post('id')
            );

            $this->edit->editRanking($dataUpdate,$dataWhere);

            echo json_encode(array(
                'success' => 'Les modifications ont été pris en compte'
            ));
        }

    }


}

