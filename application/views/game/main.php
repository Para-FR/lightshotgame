<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4 box-shadow">
                <div class="card-body">
                    <p class="card-text">Catégorie : Non définie</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" id="votePlus" class="text-white btn btn-success btn-lg btn-plus btn-outline-secondary" value="<?= $image->id ?>"><i class="far fa-thumbs-up"> </i> Like</button>
                            <button type="button" id="voteMoins" class="text-white btn btn-danger btn-lg btn-minus btn-outline-secondary"value="<?= $image->id ?>"><i class="far fa-thumbs-down"> </i> Dislike</button>
                        </div>
                        <small class="text-muted"><i class="fas fa-exclamation-triangle"></i> Signaler</small>
                    </div>
                </div>
                <img class="card-img-top" src="<?= base_url('assets/collections/') . $image->img_url ?>" alt="<?= $image->img_name ?>">
            </div>
        </div>
    </div>
</div>

