<nav class="navbar fixed-bottom navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<?= base_url() ?>">GamEpic</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home') ?>"><?php echo lang('home') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('play') ?>"><?php echo lang('play') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('ranking') ?>"><?php echo lang('ranking') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><?php echo lang('account') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/french"><img src="<?php echo base_url() . 'assets/images/banner/french.png'?>" style="width: 20px; height: 20px" alt=""></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english"><img src="<?php echo base_url() . 'assets/images/banner/english.png'?>" style="width: 20px; height: 20px" alt=""></a>
            </li>
        </ul>
    </div>
</nav>