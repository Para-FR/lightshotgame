<?php ?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <img class="mx-auto d-block mt-2" height="130px" width="100px" src="<?= base_url('assets/images/logo.png') ?>">
        </div>
    </div>
         <h1 class="text-center mt-1 mb-5">LightShotGame</h1>
<p class="text-center"> <?= $home->content_home ?></p>
</div>

<div class="container mt-5 text-center">
    <h2 class="text-center mb-5">Règles du jeu</h2>
<p><?= $home->content_rules ?></p>


    <button type="button" class="btn btn-danger"><a href="<?= base_url('play')?>"  class="text-dark">Jouer</a></button>
</div>


<div class="modal hide  text-center" id="myModal">
    <div class="modal-header">
    </div>
    <div class="modal-body">

        <h3 class="align-content-center mb-5">Warning</h3>
        <p> <?= $home->content_popup ?>
            </p> <br>

    </div>
    <div class="modal-footer">
        <a class="close" data-dismiss="modal" >J'ai plus de 18 ans</a>

    </div>
</div>