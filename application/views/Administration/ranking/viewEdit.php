<form id="form-class-edit" action="#">
    <input type="hidden" name="id" class="form-control" value="<?= $classement->id ?>" placeholder="pseudo">
    <div class="modal-header">
        <h4 class="modal-title">Modification de :  <?= $classement->img_name ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <div class="form-group m-0">
            <label for="url_edit">changement d'url' :</label>
            <input type="text" class="form-control" name="url_edit" value="<?= $classement->img_url ?>" placeholder="url">
        </div>
        <p class="field-error text-dark mb-2" data-field="url_edit"></p>
    </div>

    <div class="modal-body">
        <div class="form-group m-0">
            <label for="option_edit">changement de l'option :</label>
            <input type="text" class="form-control" name="option_edit" value="<?= $classement->img_options ?>" placeholder="option">
        </div>
        <p class="field-error text-dark mb-2" data-field="option_edit"></p>

    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-success save-edit" ><i class="fa fa-check mr-1"></i>Enregistrer</button>
    </div>
</form>