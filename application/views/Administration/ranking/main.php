
<div class="card-body" style="overflow: auto">
    <form class="form-classement">
        <div class="chart-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <table class="table table-hover table-dark text-center flex m-0">
                            <thead>
                            <tr>
                                <th>Classement</th>
                                <th>Photo</th>
                                <th>Point</th>
                                <th>Afficher</th>
                                <th>Modifier</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($images as $image) { ?>
                                <tr>
                                    <td><?= $image->cat_id ?></td>
                                    <td><img src="<?= $image->img_url ?>" width="50px" alt="<?= $image->img_url ?>"</td>
                                    <th></th>
                                    <td><a href="<?= base_url('picture/'. $image->id) ?>">aze</a></td>
                                    <td>
                                        <button type="button" class="btn btn-dark btn-show-class-edit" data-user="<?php echo $image->id; ?>">
                                            <i class="fas fa-user-cog"></i>
                                        </button>
                                        <button type="button" class="btn btn-dark btn-show-class-add" data-user="<?php echo $image->id; ?>">
                                            <i class="fas fa-plus-square"></i>
                                        </button>
                                        <button type="button" class="btn btn-dark btn-show-class-delete" data-user="<?php echo $image->id; ?>">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <canvas id="myAreaChart"> </canvas>
        </div>
    </form>
</div>
<a href="<?= base_url('ranking') ?>">Retour</a>









<div class="modal fade" id="modal-class-edit">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div> <!-- view édit -->

