
<div class="card-body" style="overflow: auto">
    <form class="form-classement">
        <div class="chart-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <table class="table table-hover table-dark text-center flex m-0">
                            <thead>
                            <tr>
                                <th>Point</th>
                                <th>Photo</th>
                                <th>Afficher</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $picture->cat_id ?></td>
                                    <td><img class="pitcureMax" src="<?= base_url('assets/collections/') . $picture->img_url ?>" alt="<?= $picture->img_url ?>"</td>
                                    <th></th>
                                    <td>A définir</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <canvas id="myAreaChart"> </canvas>
        </div>
    </form>
</div>