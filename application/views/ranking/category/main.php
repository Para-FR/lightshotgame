<form id="form-class-edit" action="#">
    <input type="hidden" name="id" class="form-control" value="<?= $ranking->id ?>" placeholder="nom">
    <div class="modal-header">
        <h4 class="modal-title">Choix de la catégorie pour :  <?= $ranking->img_name ?></h4>
        <button type="button" class="close" data-dismiss="modal"></button>
    </div>
    <div class="modal-body">
        <div class="form-group m-0">
            <label for="">Catégorie : </label>


            <?php foreach ($category as $cat){ ?>

                <p class="ml-5"><?= $cat->cat_name ?></p>


            <?php } ?>
        </div>
        <p class="field-error text-dark mb-2" data-field=""></p>
    </div>
</form>