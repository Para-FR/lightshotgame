
<div class="card-body" style="overflow: auto">
    <form class="form-classement">
    <div class="chart-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table class="table table-hover table-dark text-center flex m-0">
                        <thead>
                        <tr>
                            <th>Classement</th>
                            <th>Photo</th>
                            <th>Point</th>
                            <th>Afficher</th>
                            <th>Catégorie</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $position = 1;
                        foreach ($images as $image) { ?>

                            <tr>

                                <td class="pt-5"><?= $position ?></td>
                                <td><img src="<?= base_url('assets/collections/') . $image->img_url ?>" width="50px" alt="<?= $image->img_url ?>"</td>
                                <th class="pt-5"><?= $image->img_options ?></th>
                                <td ><a href="<?= base_url('picture/'. $image->img_name) ?>" type="button" class="btn btn-light m-4">Voir plus</a></td>
                                <td class="pt-5"> <button type="button" class="btn btn-dark btn-show-class-category" data-user="<?php echo $image->id; ?>"><i class="fas fa-compass"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php
                        $position++;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <canvas id="myAreaChart"> </canvas>
    </div>
    </form>
</div>




<div class="modal fade" id="modal-class-edit">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div> <!-- view Catégorie -->