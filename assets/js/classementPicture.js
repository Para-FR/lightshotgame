//Edit edit view
$('button.btn-show-class-edit').on('click', function () { //affiche une page en haut de page pour mettres les modifications a jours

    var user = $(this).attr('data-user');

    $.ajax({
        url: site_url + "admin/class/viewedit",
        type: 'POST',
        data: {classs_id:user},
        success: function (data) {
            if (data.error) {
                Swal.fire({
                    position : 'center',
                    icon : 'error',
                    title : 'Une erreur est survenue !',
                    showConfirmButton : false,
                    timer : 1500,
                });
            } else {
                $('#modal-class-edit .modal-content').html(data.view);
                $('#modal-class-edit').modal('show');
            }
        }
    });
});

//EDIT MODIF
$(document).on('submit', '#form-class-edit', function (e) {
    e.preventDefault();
    var formData = $('form#form-class-edit').serialize();
    var url = site_url + 'admin/class/edit';

    $.ajax({
        url : url,
        type : 'POST',
        data : formData,
        success : function (data) {

            if (data.error) {

                $('#form-class-edit p.field-error').each(function () {
                    $(this).text("");
                    for (var key in data.error) {
                        if ($(this).attr('data-field') === key) {
                            $(this).html(data.error[key]);
                        }
                    }
                })
            } else {
                $('#modal-class-edit').modal('hide');
                Swal.fire({
                    position : 'center',
                    icon : 'success',
                    title : "La Compétence a été modifié",
                    showConfirmButton : false,
                    timer : 1500,
                });
                setTimeout(function(){
                    document.location.href = site_url + "picture/admin"
                }, 1500);
            }


        }
    });
});