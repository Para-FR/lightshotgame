$('button.btn-plus').on('click', function () {
    const imgID = this.value;
    const url = site_url + 'vote';
    let elSelected = $('#votePlus');
    let elNoSelected = $('#voteMoins');

    $.ajax(
        {
            url : url,
            type : 'POST',
            data : { id : imgID },
            success: function (data) {
                if (!data.success) {
                    elSelected.html(data.error).removeClass('btn-primary').addClass('btn-danger').attr('disabled', true);
                    elNoSelected.attr('disabled', true);
                } else {
                    elSelected.html(data.success).removeClass('btn-primary').addClass('btn-success').attr('disabled', true);
                    elNoSelected.attr('disabled', true);
                }
                setTimeout(function() {
                    window.location.href = site_url + 'play';
                }, 500);
            }
            }
        );

});
$('button.btn-minus').on('click', function () {
    const imgID = this.value;
    const url = site_url + 'voteminus';
    let elSelected = $('#voteMoins');
    let elNoSelected = $('#votePlus');

    $.ajax(
        {
            url : url,
            type : 'POST',
            data : { id : imgID },
            success: function (data) {
                if (!data.success) {
                    elSelected.html(data.error).removeClass('btn-lg').addClass('btn-danger').attr('disabled', true);
                    elNoSelected.attr('disabled', true);
                } else {
                    elSelected.html(data.success).removeClass('btn-lg').addClass('btn-success').attr('disabled', true);
                    elNoSelected.attr('disabled', true);
                }
                setTimeout(function() {
                    window.location.href = site_url + 'play';
                }, 500);
            }
        }
    );

});