$('button.btn-show-class-category').on('click', function () { //affiche une page en haut de page


    var user = $(this).attr('data-user');

    $.ajax({
        url: site_url + "category",
        type: 'POST',
        data: {classs_id:user},
        success: function (data) {
            if (data.error) {
                Swal.fire({
                    position : 'center',
                    icon : 'error',
                    title : 'Une erreur est survenue !',
                    showConfirmButton : false,
                    timer : 1500,
                });
            } else {
                $('#modal-class-edit .modal-content').html(data.view);
                $('#modal-class-edit').modal('show');
            }
        }
    });
});